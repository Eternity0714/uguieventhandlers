﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveHandler : MonoBehaviour, IMoveHandler
{
    public Action onMove;
    public Action<GameObject> onMoveWithGameObject;
    public Action<GameObject, AxisEventData> onMoveWithEventData;

    public void OnMove(AxisEventData eventData)
    {
        onMove?.Invoke();
        onMoveWithGameObject?.Invoke(gameObject);
        onMoveWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onMove = null;
        onMoveWithGameObject = null;
        onMoveWithEventData = null;
    }

    public static MoveHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static MoveHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<MoveHandler>() ?? gameObject.AddComponent<MoveHandler>();
        return comp;
    }
}
