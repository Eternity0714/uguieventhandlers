﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler
{
    public Action onDrop;

    public Action<GameObject> onDropWithGameObject;

    public Action<GameObject, PointerEventData> onDropWithEventData;

    public void OnDrop(PointerEventData eventData)
    {
        onDrop?.Invoke();
        onDropWithGameObject?.Invoke(gameObject);
        onDropWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onDrop = null;
        onDropWithGameObject = null;
        onDropWithEventData = null;
    }

    public static DropHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static DropHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<DropHandler>() ?? gameObject.AddComponent<DropHandler>();
        return comp;
    }
}
