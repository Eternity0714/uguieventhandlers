﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InitializePotentialDragHandler : MonoBehaviour, IInitializePotentialDragHandler
{
    public Action onInitializePotentialDrag;
    public Action<GameObject> onInitializePotentialDragWithGameObject;
    public Action<GameObject, PointerEventData> onInitializePotentialDragWithEventData;

    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
        onInitializePotentialDrag?.Invoke();
        onInitializePotentialDragWithGameObject?.Invoke(gameObject);
        onInitializePotentialDragWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onInitializePotentialDrag = null;
        onInitializePotentialDragWithGameObject = null;
        onInitializePotentialDragWithEventData = null;
    }

    public static InitializePotentialDragHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static InitializePotentialDragHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<InitializePotentialDragHandler>() ?? gameObject.AddComponent<InitializePotentialDragHandler>();
        return comp;
    }
}
