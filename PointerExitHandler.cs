﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerExitHandler : MonoBehaviour, IPointerExitHandler
{
    public Action onPointerExit;

    public Action<GameObject> onPointerExitWithGameObject;

    public Action<GameObject, PointerEventData> onPointerExitWithEventData;

    public void OnPointerExit(PointerEventData eventData)
    {
        onPointerExit?.Invoke();
        onPointerExitWithGameObject?.Invoke(gameObject);
        onPointerExitWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onPointerExit = null;
        onPointerExitWithGameObject = null;
        onPointerExitWithEventData = null;
    }

    public static PointerExitHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static PointerExitHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<PointerExitHandler>() ?? gameObject.AddComponent<PointerExitHandler>();
        return comp;
    }
}
