﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerClickHandler : MonoBehaviour, IPointerClickHandler
{
    public bool interactable { get; set; } = true;

    public Action onPointerClick;

    public Action<GameObject> onPointerClickWithGameObject;

    public Action<GameObject, PointerEventData> onPointerClickWithEventData;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!interactable) return;

        onPointerClick?.Invoke();
        onPointerClickWithGameObject?.Invoke(gameObject);
        onPointerClickWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onPointerClick = null;
        onPointerClickWithGameObject = null;
        onPointerClickWithEventData = null;
    }

    public static PointerClickHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static PointerClickHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<PointerClickHandler>() ?? gameObject.AddComponent<PointerClickHandler>();
        return comp;
    }

}
