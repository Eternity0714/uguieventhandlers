﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
public class SubmitHandler : MonoBehaviour, ISubmitHandler
{
    public Action onSubmit;

    public Action<GameObject> onSubmitWithGameObject;

    public Action<GameObject, BaseEventData> onSubmitWithEventData;

    public void OnSubmit(BaseEventData eventData)
    {
        onSubmit?.Invoke();
        onSubmitWithGameObject?.Invoke(gameObject);
        onSubmitWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onSubmit = null;
        onSubmitWithGameObject = null;
        onSubmitWithEventData = null;
    }

    public static SubmitHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static SubmitHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<SubmitHandler>() ?? gameObject.AddComponent<SubmitHandler>();
        return comp;
    }
}
