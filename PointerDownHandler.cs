﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerDownHandler : MonoBehaviour, IPointerDownHandler
{
    public Action onPointerDown;

    public Action<GameObject> onPointerDownWithGameObject;

    public Action<GameObject, PointerEventData> onPointerDownWithEventData;

    public void OnPointerDown(PointerEventData eventData)
    {
        onPointerDown?.Invoke();
        onPointerDownWithGameObject?.Invoke(gameObject);
        onPointerDownWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onPointerDown = null;
        onPointerDownWithGameObject = null;
        onPointerDownWithEventData = null;
    }

    public static PointerDownHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static PointerDownHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<PointerDownHandler>() ?? gameObject.AddComponent<PointerDownHandler>();
        return comp;
    }
}
