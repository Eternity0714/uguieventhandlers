﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerEnterHandler : MonoBehaviour, IPointerEnterHandler
{
    public Action onPointerEnter;

    public Action<GameObject> onPointerEnterWithGameObject;

    public Action<GameObject, PointerEventData> onPointerEnterWithEventData;

    public void OnPointerEnter(PointerEventData eventData)
    {
        onPointerEnter?.Invoke();
        onPointerEnterWithGameObject?.Invoke(gameObject);
        onPointerEnterWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onPointerEnter = null;
        onPointerEnterWithGameObject = null;
        onPointerEnterWithEventData = null;
    }

    public static PointerEnterHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static PointerEnterHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<PointerEnterHandler>() ?? gameObject.AddComponent<PointerEnterHandler>();
        return comp;
    }
}
