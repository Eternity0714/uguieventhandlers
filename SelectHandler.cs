﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
public class SelectHandler : MonoBehaviour, ISelectHandler
{
    public Action onSelect;

    public Action<GameObject> onSelectWithGameObject;

    public Action<GameObject, BaseEventData> onSelectWithEventData;

    public void OnSelect(BaseEventData eventData)
    {
        onSelect?.Invoke();
        onSelectWithGameObject?.Invoke(gameObject);
        onSelectWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onSelect = null;
        onSelectWithGameObject = null;
        onSelectWithEventData = null;
    }

    public static SelectHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static SelectHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<SelectHandler>() ?? gameObject.AddComponent<SelectHandler>();
        return comp;
    }
}
