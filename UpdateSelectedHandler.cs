﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UpdateSelectedHandler : MonoBehaviour, IUpdateSelectedHandler
{
    public Action onUpdateSelected;

    public Action<GameObject> onUpdateSelectedWithGameObject;

    public Action<GameObject, BaseEventData> onUpdateSelectedWithEventData;

    public void OnUpdateSelected(BaseEventData eventData)
    {
        onUpdateSelected?.Invoke();
        onUpdateSelectedWithGameObject?.Invoke(gameObject);
        onUpdateSelectedWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onUpdateSelected = null;
        onUpdateSelectedWithGameObject = null;
        onUpdateSelectedWithEventData = null;
    }

    public static UpdateSelectedHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static UpdateSelectedHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<UpdateSelectedHandler>() ?? gameObject.AddComponent<UpdateSelectedHandler>();
        return comp;
    }
}
