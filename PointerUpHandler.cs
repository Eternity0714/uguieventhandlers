﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PointerUpHandler : MonoBehaviour, IPointerUpHandler
{
    public Action onPointerUp;

    public Action<GameObject> onPointerUpWithGameObject;

    public Action<GameObject, PointerEventData> onPointerUpWithEventData;

    public void OnPointerUp(PointerEventData eventData)
    {
        onPointerUp?.Invoke();
        onPointerUpWithGameObject?.Invoke(gameObject);
        onPointerUpWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onPointerUp = null;
        onPointerUpWithGameObject = null;
        onPointerUpWithEventData = null;
    }

    public static PointerUpHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static PointerUpHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<PointerUpHandler>() ?? gameObject.AddComponent<PointerUpHandler>();
        return comp;
    }

}
