﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler
{
    public Action onDrag;

    public Action<GameObject> onDragWithGameObject;

    public Action<GameObject, PointerEventData> onDragWithEventData;

    public void OnDrag(PointerEventData eventData)
    {
        onDrag?.Invoke();
        onDragWithGameObject?.Invoke(gameObject);
        onDragWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onDrag = null;
        onDragWithGameObject = null;
        onDragWithEventData = null;
    }

    public static DragHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static DragHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<DragHandler>() ?? gameObject.AddComponent<DragHandler>();
        return comp;
    }
}
