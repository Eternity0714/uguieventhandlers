﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DeselectHandler : MonoBehaviour, IDeselectHandler
{
    public Action onDeselect;

    public Action<GameObject> onDeselectWithGameObject;

    public Action<GameObject, BaseEventData> onDeselectWithEventData;

    public void OnDeselect(BaseEventData eventData)
    {
        onDeselect?.Invoke();
        onDeselectWithGameObject?.Invoke(gameObject);
        onDeselectWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onDeselect = null;
        onDeselectWithGameObject = null;
        onDeselectWithEventData = null;
    }

    public static DeselectHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static DeselectHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<DeselectHandler>() ?? gameObject.AddComponent<DeselectHandler>();
        return comp;
    }
}
