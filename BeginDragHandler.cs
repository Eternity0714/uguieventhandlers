﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeginDragHandler : MonoBehaviour, IBeginDragHandler
{
    public Action onBeginDrag;

    public Action<GameObject> onBeginDragWithGameObject;

    public Action<GameObject, PointerEventData> onBeginDragWithEventData;

    public void OnBeginDrag(PointerEventData eventData)
    {
        onBeginDrag?.Invoke();
        onBeginDragWithGameObject?.Invoke(gameObject);
        onBeginDragWithEventData?.Invoke(gameObject, eventData);
    }

    void OnDestroy() {
        onBeginDrag = null;
        onBeginDragWithGameObject = null;
        onBeginDragWithEventData = null;
    }

    public static BeginDragHandler Get(Component component) {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static BeginDragHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<BeginDragHandler>() ?? gameObject.AddComponent<BeginDragHandler>();
        return comp;
    }
}
