﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollHandler : MonoBehaviour, IScrollHandler
{
    public Action onScroll;

    public Action<GameObject> onScrollWithGameObject;

    public Action<GameObject, PointerEventData> onScrollWithEventData;

    public void OnScroll(PointerEventData eventData)
    {
        onScroll?.Invoke();
        onScrollWithGameObject?.Invoke(gameObject);
        onScrollWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onScroll = null;
        onScrollWithGameObject = null;
        onScrollWithEventData = null;
    }

    public static ScrollHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static ScrollHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<ScrollHandler>() ?? gameObject.AddComponent<ScrollHandler>();
        return comp;
    }

}
