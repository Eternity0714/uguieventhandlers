﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class EndDragHandler : MonoBehaviour, IEndDragHandler
{
    public Action onEndDrag;
    public Action<GameObject> onEndDragWithGameObject;

    public Action<GameObject, PointerEventData> onEndDragWithEventData;

    public void OnEndDrag(PointerEventData eventData)
    {
        onEndDrag?.Invoke();
        onEndDragWithGameObject?.Invoke(gameObject);
        onEndDragWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onEndDrag = null;
        onEndDragWithGameObject = null;
        onEndDragWithEventData = null;
    }

    public static EndDragHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static EndDragHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<EndDragHandler>() ?? gameObject.AddComponent<EndDragHandler>();
        return comp;
    }
}
