﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CancelHandler : MonoBehaviour, ICancelHandler
{
    public Action onCancel;

    public Action<GameObject> onCancelWithGameObject;

    public Action<GameObject, BaseEventData> onCancelWithEventData;

    public void OnCancel(BaseEventData eventData)
    {
        onCancel?.Invoke();
        onCancelWithGameObject?.Invoke(gameObject);
        onCancelWithEventData?.Invoke(gameObject, eventData);
    }

    private void OnDestroy()
    {
        onCancel = null;
        onCancelWithGameObject = null;
        onCancelWithEventData = null;
    }

    public static CancelHandler Get(Component component)
    {
        var gameObject = component.gameObject;
        return Get(gameObject);
    }

    public static CancelHandler Get(GameObject gameObject)
    {
        var comp = gameObject.GetComponent<CancelHandler>() ?? gameObject.AddComponent<CancelHandler>();
        return comp;
    }
}
