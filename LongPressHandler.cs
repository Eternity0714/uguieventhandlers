﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LongPressHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private const float DEFAULT_DELTA = 0.5f;

    public Action<GameObject, PointerEventData> onPointerDown;

    public Action<GameObject, PointerEventData> onPointerUp;

    public Action<GameObject, PointerEventData> onPointerClick;

    public Action<GameObject> onLongPress;

    bool isPointerDown = false;

    bool isDraging = false;

    DateTime time;

    public void OnPointerDown(PointerEventData eventData)
    {
        onPointerDown?.Invoke(gameObject, eventData);
        isPointerDown = true;
        time = DateTime.Now;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onPointerUp?.Invoke(gameObject, eventData);
        isPointerDown = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        isPointerDown = false;
    }

    void Update()
    {
        if (!isPointerDown) return;
        if (DateTime.Now.AddSeconds(DEFAULT_DELTA) >= time) {
            onLongPress?.Invoke(gameObject);
        }
    }

    public static LongPressHandler Get(Component component) {
        var go = component.gameObject;
        return Get(go);
    }

    public static LongPressHandler Get(GameObject gameObject) {
        var comp = gameObject.GetComponent<LongPressHandler>() ?? gameObject.AddComponent<LongPressHandler>();
        return comp;
    }
}
